#!/bin/bash
export JAVA_HOME="C:/Program Files/Java/jdk1.8.0_271"
echo $JAVA_HOME

echo "Running Maven clean and Package"
mvn clean package
echo "================================"
echo "================================"

echo "Uploading file to S3 bucket and packing file to be deployed"
aws cloudformation package --template-file template.yml --output-template-file output-sam.yml --s3-bucket springboot-aws-cpinto
echo "================================"
echo "================================"
echo "Uploading and deploying the lambda"
aws cloudformation deploy --template-file output-sam.yml --stack-name springboot-aws-app --capabilities CAPABILITY_IAM
echo "================================"
echo "================================"

echo "Success."