package com.example.leira.controller;


import com.example.leira.model.Test;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("file")
public class FileController {

    @Value("classpath:templates/text.txt")
    Resource resourceFile;

    @PostMapping(value = "/get")
    public String getFile(HttpServletRequest request, Test test) throws IOException {
        System.out.println( ResourceUtils.getFile("classpath:application.properties").getAbsolutePath());;
        List<String> headers = Collections.list(request.getHeaderNames());
        System.out.println(Arrays.toString(headers.toArray()));
        BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
        Map event = new ObjectMapper().readValue(reader, Map.class);
        System.out.println(event);
        System.out.println(new ObjectMapper().writeValueAsString(test));
        return Files.readAllLines(resourceFile.getFile().toPath()).toString();
    }

}
